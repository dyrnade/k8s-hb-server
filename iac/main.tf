provider "google" {
}

data "google_container_engine_versions" "europe-west" {
  version_prefix = "1.15."
}

resource "google_container_cluster" "hb-gke-cluster" {
  name               = "hb-gke-cluster"
  node_version       = data.google_container_engine_versions.europe-west.latest_node_version
  min_master_version = data.google_container_engine_versions.europe-west.latest_node_version

  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "hb-gke-cluter-nodes" {
  name       = "hb-node-pool"
  cluster    = google_container_cluster.hb-gke-cluster.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "g1-small"

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
    ]
  }
}
