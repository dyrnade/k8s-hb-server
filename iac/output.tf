output "cluster_username" {
  value = google_container_cluster.hb-gke-cluster.master_auth[0].username
}

output "cluster_password" {
  value = google_container_cluster.hb-gke-cluster.master_auth[0].password
}

output "endpoint" {
  value = google_container_cluster.hb-gke-cluster.endpoint
}

output "instance_group_urls" {
  value = google_container_cluster.hb-gke-cluster.instance_group_urls
}

output "node_config" {
  value = google_container_cluster.hb-gke-cluster.node_config
}

output "node_pools" {
  value = google_container_cluster.hb-gke-cluster.node_pool
}
