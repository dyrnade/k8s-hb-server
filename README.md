Project link: https://gitlab.com/dyrnade/k8s-hb-server

Prerequesites:
  - gitlab account
  - gcloud account
  - kubectl version v1.16.6-beta.0
  - helm version v3.2.4
  - Terraform v0.12.25
  - Google Cloud SDK 306.0.0

Create a service account with Kubernetes Engine Admin role on Gcloud Dashboard.

The project has three parts in terms of code.
  - iac: The infrastructure code which will deploy Kubernetes cluster on GCloud.
  - deployments: Kubernetes cluster deployment.
  - charts: Custom helm charts.

IMPORTANT: You should set GCLOUD_KEYFILE_JSON variable(type: file) in https://gitlab.com/USERNAME/k8s-hb-server/-/settings/ci_cd with credentials file's content(Json) you downloaded from https://console.cloud.google.com/iam-admin/serviceaccounts?project=GOOGLE_PROJECT
